using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace NavigateGooglePicturesTests
{
    public class Tests
    {
        private IWebDriver driver;
        private readonly By _searchField = By.CssSelector("div.SDkEP input[class='gLFyf gsfi']");
        private readonly By _googleSearchButton = By.CssSelector("div[class='FPdoLc lJ9FBc'] input[name='btnK']");
        private readonly By imageResult = By.CssSelector("div.eA0Zlc");
        private const string searchImage = "Image";

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://google.com");
            driver.Manage().Window.Maximize(); 
        }

        [Test]
        public void Test1()
        {
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
            IWebElement googleSearch = wait.Until(e => e.FindElement(_searchField));
            googleSearch.SendKeys(searchImage);
            IWebElement googleSearchButton = wait.Until(e => e.FindElement(_googleSearchButton));
            googleSearchButton.SendKeys(Keys.Enter);

            IWebElement result = wait.Until(e => e.FindElement(imageResult));

            Assert.That(result.Displayed, Is.True);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}